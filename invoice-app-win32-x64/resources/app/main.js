const {app, BrowserWindow, ipcMain} = require('electron');
let fs=require('fs');
const {ipcRenderer} = require('electron');

let win = null;

app.on('ready', function () {
  

  // Initialize the window to our specified dimensions
  win = new BrowserWindow({width: 1000, height: 600});
  win.maximize();
  // Specify entry point
  win.loadURL('http://localhost:8888/');

  // Show dev tools
  // Remove this line before distributing
  // win.webContents.openDevTools()

  // Remove window once app is closed
  // win.on('closed', function (e) {
  //   e.preventDefault();
  //   console.log('wind closee');
  //   // win = null;
  // });
  win.on('close', (e) => {
    win=null;
      // e.preventDefault() ;
      // win.webContents.send('info','test msg');
      
  })

});

app.on('activate', () => {
  if (win === null) {
    createWindow()
  }
})

ipcMain.on('store-data', (event, localstroage) => {  
  let date=new Date();
  var dir = './data';
  if (!fs.existsSync(dir)){
      fs.mkdirSync(dir);
  }
  let fileName=  `${dir}/${Date.now()}~${date.getDate()}-${date.getMonth()}-${date.getFullYear()}-${date.getMinutes()}-${date.getSeconds()}.json`;
  
  fs.writeFile(fileName,localstroage,(err)=>{
    if (err) throw err;
    console.log(fileName+ ' file saved');
  });

});

ipcMain.on('get-latest-data', (event, localstroage) => { 
  console.log('get latete data')
  var dir = './data';
  var latestFileName='';
  var latest=0;

  if (!fs.existsSync(dir)){
    return;
  } 

  var  files= fs.readdirSync(dir);
  files.forEach(file => {
    let name =parseInt(file.split('~')[0]);
    if(latest < name){
      latest=name;
      latestFileName=file;
    }
  });
  let filePath= `${dir}/${latestFileName}`;
  var fileData= fs.readFileSync(filePath,'utf-8');
  
  win.webContents.send('set-latest-data', fileData);
});

app.on('window-all-closed', function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow()
  }
});


/* server don touch here after */

var http = require("http"),
    url = require("url");
    const path = require("path");
   const port = process.argv[2] || 8888;

http.createServer(function(request, response) {

  var uri = url.parse(request.url).pathname;
  var filename = path.join(__dirname+'/dist', uri);
  
  fs.exists(filename, function(exists) {
    if(!exists) {
      response.writeHead(404, {"Content-Type": "text/plain"});
      response.write("404 Not Found\n");
      response.end();
      return;
    }

    if (fs.statSync(filename).isDirectory()) filename += '/index.html';

    fs.readFile(filename, "binary", function(err, file) {
      if(err) {        
        response.writeHead(500, {"Content-Type": "text/plain"});
        response.write(err + "\n");
        response.end();
        return;
      }

      response.writeHead(200);
      response.write(file, "binary");
      response.end();
    });
  });
}).listen(parseInt(port, 10));

console.log("Static file server running at\n  => http://localhost:" + port + "/\nCTRL + C to shutdown");
