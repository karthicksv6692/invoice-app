import { Component } from '@angular/core';


@Component({
  selector: 'footer',
  template: `<section class="foot_panels">
                <div class="foot_tab">
                <table class="table table-bordered tab_chan">
                    <thead class="tab_heds">
                    <tr class="tab_trs">
                        <td class="tab_ths"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Invoice Printer 2018</font></font></td>
                        <td class="tab_ths"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">EUR</font></font></td>
                        <td class="tab_ths"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">20: 10 am</font></font></td>
                        <td class="tab_ths"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Mittch 13 april 2018</font></font></td>
                        <td class="tab_ths"></td>
                        <td class="tab_ths"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Datasets: 25 out of 25</font></font></td>
                    </tr>
                    </thead>
                    </table>
                </div>
            </section>`,
})
export class FooterComponent {
}


