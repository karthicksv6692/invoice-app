import { Component } from '@angular/core';


@Component({
  selector: 'header-2',
  template: `<div class="Vr_header2">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <div class="Vr_header2">
                        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 pads">
                            <div class="vr_menu_change1">
                                <p class="header_menu1"><a class="mds" href="#">Datei</a></p>
                            </div>
                        </div>
                        <div class="col-lg-10 col-md-10 col-sm-6 col-xs-6">
                            <div class="vr_menu_change2">
                                <p class="header_menu2"><a class="mds" href="#">Diverses</a></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <div class="Vr_header3">
                        <ul class="Vr_header3_1">
                            <li class="Vr_icon"><a class="mds" href="#"><p class="header_menu3"><i class="fa fa-user-plus icons_head blues"></i></p></a></li>
                            <li class="Vr_icon"><a class="mds" href="#"><p class="header_menu4"><i class="fa fa-question-circle icons_head red"></i>Hife</p></a></li>
                            <li class="Vr_icon1"><a class="mds" href="#"><p class="header_menu5"><i class="fa fa-info-circle icons_head blue"></i>Info</p></a></li>
                        </ul>
                    </div>
                </div>
            </div>`,
})
export class HeaderTwoComponent {
}
