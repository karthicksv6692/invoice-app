import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ElectronService } from 'ngx-electron';

@Component({
  selector: 'app-bill-list',
  templateUrl: './bill-list.component.html',
  styleUrls: ['./bill-list.component.css']
})
export class BillListComponent implements OnInit {

  tab_data:any = [];
  json_data:any=[];
  private data:Array<any>

  public rows:Array<any> = [];
  public columns:Array<any> = [
    {title: 'Rechnungensnr', name: 'invoice_number'},
    { title: 'Projectnr', name: 'project_number'},
    {title: 'Datum',  name: 'date', },
    {title: 'Formular', name: 'form_type',  },
    {title: 'Kd-nr',  name: 'customer_number'},
    {title: 'Kunde', name: 'customer_name'},
    {title: 'Mitarbeiter', name: 'emp_name'},
    {title: 'Nettobegrag', name: 'total'},
    {title: 'M w st', name: 'total'},
    
  ];
  public page:number = 1;
  public itemsPerPage:number = 10;
  public maxSize:number = 5;
  public numPages:number = 1;
  public length:number = 0;

  public config:any = {
    paging: true,
    sorting: {columns: this.columns},
    filtering: {filterString: ''},
    className: ['table-striped', 'table-bordered']
  };

  constructor(private router:Router,private route: ActivatedRoute,private _electronService: ElectronService){
    //  debugger;
    
  }

  public ngOnInit():void {
    

    // if(this._electronService.isElectronApp){
    //   this._electronService.ipcRenderer.on('set-latest-data', (e,data)=>{
    //     console.log('json data', data)
    //     if(data){
    //       this.json_data=data;
    //     }
    //   });

    //   this._electronService.ipcRenderer.send('get-latest-data','get latest');
    // }

    if(localStorage.getItem('table_data')){
        try {
          this.tab_data = JSON.parse(localStorage.getItem('table_data'));
        } catch (e) {
          this.tab_data =[];
        }
        this.data = this.tab_data;
    }else{
      this.data = [];
       if(this.json_data){
         this.tab_data=this.json_data
       }else{
         this.tab_data =[];
       }
    }
    this.onChangeTable(this.config);
  }
    
   goToAdd(){
     this.router.navigateByUrl(`bill/(inner:add)`);
   }
   billSview(id){
    this.router.navigateByUrl(`bill/(inner:sview/${id})`);
   }

  public changePage(page:any, data:Array<any> = this.data):Array<any> {
    let start = (page.page - 1) * page.itemsPerPage;
    let end = page.itemsPerPage > -1 ? (start + page.itemsPerPage) : data.length;
    return data.slice(start, end);
  }

  public changeSort(data:any, config:any):any {
    if (!config.sorting) {
      return data;
    }

    let columns = this.config.sorting.columns || [];
    let columnName:string = void 0;
    let sort:string = void 0;

    for (let i = 0; i < columns.length; i++) {
      if (columns[i].sort !== '' && columns[i].sort !== false) {
        columnName = columns[i].name;
        sort = columns[i].sort;
      }
    }

    if (!columnName) {
      return data;
    }

    // simple sorting
    return data.sort((previous:any, current:any) => {
      if (previous[columnName] > current[columnName]) {
        return sort === 'desc' ? -1 : 1;
      } else if (previous[columnName] < current[columnName]) {
        return sort === 'asc' ? -1 : 1;
      }
      return 0;
    });
  }

  public changeFilter(data:any, config:any):any {
    let filteredData:Array<any> = data;
      this.columns.forEach((column:any) => {
        if (column.filtering) {
          filteredData = filteredData.filter((item:any) => {
            return item[column.name].match(column.filtering.filterString);
          });
        }
      });
  
      if (!config.filtering) {
        return filteredData;
      }
  
      if (config.filtering.columnName) {
        return filteredData.filter((item:any) =>
          item[config.filtering.columnName].match(this.config.filtering.filterString));
      }
  
      let tempArray:Array<any> = [];
      filteredData.forEach((item:any) => {
        let flag = false;
        this.columns.forEach((column:any) => {
          if (item[column.name].toString().match(this.config.filtering.filterString)) {
            flag = true;
          }
        });
        if (flag) {
          tempArray.push(item);
        }
      });
      filteredData = tempArray;
  
      return filteredData;
  }

  public onChangeTable(config:any, page:any = {page: this.page, itemsPerPage: this.itemsPerPage}):any {
    if (config.filtering) {
      Object.assign(this.config.filtering, config.filtering);
    }

    if (config.sorting) {
      Object.assign(this.config.sorting, config.sorting);
    }

    let filteredData = this.changeFilter(this.data, this.config);
    let sortedData = this.changeSort(filteredData, this.config);
    this.rows = page && config.paging ? this.changePage(page, sortedData) : sortedData;
    this.length = sortedData.length;
  }

  public onCellClick(data: any): any {
    this.router.navigateByUrl(`bill/(inner:sview/${data.row['invoice_number']})`);
  }

}
