import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BillService } from './shared/bill.services';

@Component({
  selector: 'bill-sview',
  templateUrl: './bill_single.component.html',
  styles:['.bbd{margin:25px;}.pad_zero{padding:0px}.rr{width:100%;clear:both;padding: 5px 0px;}']
})
export class BillSingleComponent {
    id:number;
    bill:any
    res:any
    constructor(private router:Router,private studentService:BillService,private route: ActivatedRoute) {
        this.route.params.subscribe(params => {
            // console.log(params) //log the entire params object
            // console.log(params['id']) //log the value of id
            this.id = params['id'];
        });
        this.res = studentService.getSingleBill(this.id);
        console.log(this.res);
        
        if(!isNaN(this.res)){
            this.router.navigateByUrl(`bill/(inner:list)`);
        }else{
            this.bill = this.res;
        }
    }
    goBack(){
        this.router.navigateByUrl(`bill/(inner:list)`);
    }
}
