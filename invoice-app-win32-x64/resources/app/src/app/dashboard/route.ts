import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './dashboard.component';
import { BillComponent } from './bill.component';
import { BillAddComponent } from './bill-add.component';
import { BillSingleComponent } from './bill_single.component';
import { CustomerAddComponent } from './customer/customer_add.component';
import { BillListComponent } from './bill-list/bill-list.component';
import { NgTableExampleComponent } from './ng-table-example/ng-table-example.component';

const dashRoutes: Routes = [
  { path: '', component: DashboardComponent, pathMatch: 'full' },
  { path: 'dashboard', component: DashboardComponent, pathMatch: 'full' },
  { path: 'bill', component: BillComponent,
    children:[
      { path: 'list', component: BillListComponent, outlet:'inner'},
      { path: 'add', component: BillAddComponent, outlet:'inner'},
      { path: 'sview/:id', component: BillSingleComponent, outlet:'inner'},
    ]
 },
 
  //{ path: 'bill/add', component: BillAddComponent },
  // { path: 'bill/:id', component: BillSingleComponent },
  { path: 'customer/add', component: CustomerAddComponent },
  { path: '', pathMatch: 'full', component: BillComponent},
];

export const dashRouting = RouterModule.forChild(dashRoutes);
