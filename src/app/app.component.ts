import { Component, OnInit } from '@angular/core';
import * as myGlobals from './common-variables'
import {ElectronService} from 'ngx-electron';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = myGlobals.domainName;
  constructor(private _electronService: ElectronService) { }

  ngOnInit(){
    if(this._electronService.isElectronApp){
      // this._electronService.ipcRenderer.send('asynchronous-message', localStorage.table_data);
      this._electronService.ipcRenderer.on('info', (event, arg) => {
        this._electronService.ipcRenderer.send('store-data', localStorage.table_data? localStorage.table_data: false);
         
      });
    }

    /* if(this._electronService.isElectronApp){
      this._electronService.ipcRenderer.on('set-latest-data', (e,data)=>{
        console.log('json data', data)
        if(data){
          this.json_data=data;
        }
      });

      this._electronService.ipcRenderer.send('get-latest-data','get latest');
    } */
  }

}
