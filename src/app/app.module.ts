import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { LoginComponent } from './login.component';
import { DashboardModule } from './dashboard/dashboard.module';

import { routing } from './app.route';
import { dashRouting } from './dashboard/route';

import {SelectModule} from 'ng2-select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxElectronModule } from 'ngx-electron';


@NgModule({
  declarations: [
    AppComponent,LoginComponent
  ],
  imports: [
    BrowserModule,DashboardModule,routing,dashRouting,SelectModule,FormsModule,ReactiveFormsModule,
    NgxElectronModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
