import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule }   from '@angular/router';

import { LoginComponent } from './login.component';

const appRoutes: Routes = [
  { path: 'login', pathMatch: 'full', component: LoginComponent },
  { path: 'not-found', component: LoginComponent },
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
