import { Component } from '@angular/core';


@Component({
  selector: 'header-1',
  template: `<div class="Vr_header1">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="Vr_header_sub">
                        <p class="vr_header_title">
                            Rechnungsdruckerei 2018
                        </p>
                    </div>
                </div>
            </div>`,
})
export class HeaderOneComponent {
}
