import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'top-menu',
  templateUrl: 'top-menu.component.html',
})
export class TopMenuComponent {
  constructor(private router:Router,private route: ActivatedRoute){}
  goAddNew(){
    this.router.navigateByUrl(`bill/(inner:add)`);
  }
}
