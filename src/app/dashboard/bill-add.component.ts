import { Component,OnInit, 
  ViewChild, ElementRef ,AfterViewInit, ViewChildren, NgZone
    } from '@angular/core';
import { BillService } from './shared/bill.services';
import { BillProductComponent } from './bill-product.component';
import {NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import * as jsPDF from 'jspdf'
import { DELEGATE_CTOR } from '@angular/core/src/reflection/reflection_capabilities';
import * as html2canvas from 'html2canvas';
import { ElectronService } from 'ngx-electron';

declare var $:any;
@Component({
  selector: 'bill',
  templateUrl: './bill-add.component.html',
  styles:[`
      element.style {
          outline: 0;
      }
      .ui-select-match[_ngcontent-c2] > .btn[_ngcontent-c2] {
          text-align: left !important;
      }
      .btn-default.active.focus, .btn-default.active:focus, .btn-default.active:hover, .btn-default:active.focus, .btn-default:active:focus, .btn-default:active:hover, .open>.dropdown-toggle.btn-default.focus, .open>.dropdown-toggle.btn-default:focus, .open>.dropdown-toggle.btn-default:hover {
          color: #333;
          background-color: #fff !important;
          border-color: #777272;
      }
      .pad_zero{padding:0px}
      .rr{width:100%;clear:both;padding: 5px 0px;}
      .margin_zero{margin:0px;}`
  ]
})
export class BillAddComponent implements OnInit,AfterViewInit{ 

  @ViewChild('prd_table') _elementref:ElementRef;
  @ViewChild('ppp') _focref:ElementRef;
  @ViewChild('cntt') cntt:ElementRef;

  rectW:number = 100;
  rectH:number = 100;
  rectColor:string = "#FF0000";
  context:CanvasRenderingContext2D;
  
  table_ar:any = [];
  customer:any[];
  products:any[];
  product_names:any[];
  employee:any[];
  payment_type:any[];
  invoice_number:string;
  project_number:string;
  customer_number:string;
  today_date:string;
  stu:any;
  i:number = 0;
  selectedDevice:number = 0;
  total:number = 0;
  shipping:number = 50;
  total_tax:number = 0;
  total_net:number = 0;
  dis:number = 0;
  grand_total:number = 0;
  message;
  
  constructor(private studentService:BillService,private router:Router,
    private _electronService: ElectronService, private _ngZone: NgZone){

    this.customer = studentService.getCustomer();
    this.products = studentService.getProducts();
    this.product_names = studentService.getProductNames();
    this.employee = studentService.getEmployee();
    this.payment_type = studentService.getPaymentType();
    this.invoice_number = studentService.getInvoiceNo();
    this.project_number = studentService.getProjectNo();
    this.today_date = studentService.getTodayDate();
    this.stu = studentService;
    this.customer_number='';
  }

  playPingPong() {
    if(this._electronService.isElectronApp){
      this._electronService.ipcRenderer.send('asynchronous-message', 'ping');
    }
  }

  ngOnInit(){
    this.products = this.stu.getProductNames();
  }
  appendInit(id){
    var that=this;
    var xz=[];
  
    xz=this._elementref.nativeElement.querySelector('.del_but_'+id).addEventListener('click',function(e:any){
      that.removeRow(e.target.id);
    });
  }
  ngAfterViewInit(){
    this.addFieldValue();
    // this.vc.first.nativeElement.focus()
    // this.vcc.first.nativeElement.focus()
  }

  addComponent(){
    var table:any = document.getElementById("product_details_bdy");
    var x:number = document.getElementById("product_details").getElementsByTagName('tr').length;
    var row = table.insertRow(x-1);
    var cell1 = row.insertCell(0); var cell2 = row.insertCell(1); var cell3 = row.insertCell(2);
    var cell4 = row.insertCell(3);var cell5 = row.insertCell(4); var cell6 = row.insertCell(5); var cell7 = row.insertCell(6);
    var cell8 = row.insertCell(7); var cell9 = row.insertCell(8);
    var i=this.i;
    var htm:string = "<select>";
      for(i=0;i<this.products.length;i++){
        htm += "<option>"+this.products[i]['name']+"</option>";
      }
    htm += "</select>";
    cell1.innerHTML = x; 
    cell2.innerHTML = htm; 
    cell3.innerHTML = ""; 
    cell4.innerHTML = "";
    cell5.innerHTML = ""; 
    cell6.innerHTML = ""; 
    cell7.innerHTML = ""; 
    cell8.innerHTML = "";
    cell9.innerHTML = "<input type='button' class='del_but_"+x+"' value='Delete' id="+x+">";
    this.appendInit(x);
  }
  removeRow(id){
    console.log(id);
  }

  private fieldArray: Array<any> = [];
  private newAttribute: any = {};

  addFieldValue() {
    this.newAttribute = {name:'',desc:'',price:'0.00',unit:1,e_tot:'0.00',tax:'0.00',grand_tot:0};
    this.fieldArray.push(this.newAttribute);
  }

  deleteFieldValue(index) {
    var cc = 0;
    var yy:number;
    this.fieldArray.splice(index, 1);
    this.fieldArray.forEach(function(element) {
      cc += element.grand_tot
    });
    this.total = cc;
    this.total_tax = cc*10/100;
    this.total_net = cc+cc*10/100+this.shipping;
    this.grand_total = this.total_net+this.dis;
  }
  prdChange(val,index,unit){
    var cc = 0;
    var prd = this.stu.getSingleProductDetails(val); 
    
    this.fieldArray[index].id = prd.id;
    this.fieldArray[index].name = prd.name;
    this.fieldArray[index].desc = prd.desc;
    this.fieldArray[index].price = prd.price;
    this.fieldArray[index].unit = parseInt(unit);
    this.fieldArray[index].e_tot =  prd.price*parseInt(unit);
    this.fieldArray[index].tax =  (prd.price*parseInt(unit))*5/100;
    this.fieldArray[index].grand_tot =  (prd.price*parseInt(unit))+prd.price*5/100;

    this.fieldArray.forEach(function(element) {
      cc += element.grand_tot
    });
    this.total = cc;
    this.total_tax = cc*10/100;
    this.total_net = cc+cc*10/100+this.shipping;
    this.grand_total = this.total_net+this.dis;
  }

  quantityChange(prd,val,index){
    var cc = 0;
    if(prd>0){
      var prd = this.stu.getSingleProductDetails(prd); 
      this.fieldArray[index].price = prd.price;
      this.fieldArray[index].unit = parseInt(val);
      this.fieldArray[index].e_tot =  prd.price*parseInt(val);
      this.fieldArray[index].tax =  (prd.price*parseInt(val))*5/100;
      this.fieldArray[index].grand_tot =  (prd.price*parseInt(val))+prd.price*5/100;

      this.fieldArray.forEach(function(element) {
        cc += element.grand_tot
      });
      this.total = cc;
      this.total_tax = cc*10/100;
      this.total_net = cc+cc*10/100+this.shipping;
      this.grand_total = this.total_net+this.dis;
    }else{
      this.fieldArray[index].unit = val;
    }
    
  }
  discountChange(val){
    this.dis = parseInt(val);
    this.grand_total =this.total_net+ parseInt(val);
  }
  
  private value:any = {};
  public selected(value:any):void {
    this.customer_number = this.stu.getSingleCustomer(value.id)
  }
 
  public removed(value:any):void {
    this.customer_number = '';
  }
 
  public refreshValue(value:any):void {
    this.value = value;
  }
  formSubmit(val:NgForm) {
    // console.log(val.value);
    this.table_ar.push({
      invoice_number: val.value['invoice_number'],
      project_number : val.value['project_number'],
      date : val.value['today_date'],
      form_type : val.value['form_type'],
      customer_number : this.customer_number,
      customer_name : this.customer_number,
      emp_name : val.value['emp_name'],
      total : this.total,
      grand_total : this.grand_total,
      delivery_place : val.value['delivery_place'],
      greet_msg : val.value['greet_msg'],
      top_greet_msg : val.value['top_greet_msg'],
      zahlungsbed : val.value['zahlungsbed'],
      payment_type : val.value['payment_type'],
      products:this.fieldArray,
      shipping : this.shipping,
      total_tax : this.total_tax,
      total_net : this.total_net,
      dis : this.dis,
    })  
    try {
      var ar = JSON.parse(localStorage.getItem('table_data'));
      if(ar){
        var rr = this.table_ar.concat(ar);
      }else{
        var rr = this.table_ar;
      }
    } catch (e) {
      var rr = this.table_ar;
    }
   this.playPingPong();
    localStorage.setItem('table_data', JSON.stringify(rr));

    this.router.navigateByUrl(`bill/(inner:list)`);
  }

  prd_select(val,index,unit){
    var cc = 0;
    var prd = this.stu.getSingleProductDetails(val.id); 
    
    this.fieldArray[index].id = prd.id;
    this.fieldArray[index].name = prd.name;
    this.fieldArray[index].desc = prd.desc;
    this.fieldArray[index].price = prd.price;
    this.fieldArray[index].unit = parseInt(unit);
    this.fieldArray[index].e_tot =  prd.price*parseInt(unit);
    this.fieldArray[index].tax =  (prd.price*parseInt(unit))*5/100;
    this.fieldArray[index].grand_tot =  (prd.price*parseInt(unit))+prd.price*5/100;

    this.fieldArray.forEach(function(element) {
      cc += element.grand_tot
    });
    this.total = cc;
    this.total_tax = cc*10/100;
    this.total_net = cc+cc*10/100+this.shipping;
    this.grand_total = this.total_net+this.dis;
    // this.vc.first.nativeElement.focus();
    // debugger;
    this._focref.nativeElement.addEventListener
    const focuss:any=document.querySelector("#unit_"+index);
    
    focuss.focus();
  }
  printPdf(){
    // html2canvas(this.cntt.nativeElement, <Html2Canvas.Html2CanvasOptions>{
    //   onrendered: function(canvas: HTMLCanvasElement) {
    //     var pdf = new jsPDF('p','pt','a4');

    //     pdf.addHTML(canvas, function() {
    //       pdf.save('web.pdf');
    //     });
    //   }  
    // });


    // html2canvas(this.cntt.nativeElement, <Html2Canvas.Html2CanvasOptions>{
    //   onrendered: function(canvas: HTMLCanvasElement) {
    //     var pdf = new jsPDF('p','pt','a4');

    //     pdf.addHTML(canvas, function() {
    //       pdf.save('web.pdf');
    //     });
    //   }
    // });

    

    let doc = new jsPDF();

    
  }
  addCustomerRoute(){
    this.router.navigate(['/customer/add'])
  }
  
  goListPage(){
    this.router.navigateByUrl(`bill/(inner:list)`);
  }

}
