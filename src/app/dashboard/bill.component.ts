import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Route } from '@angular/compiler/src/core';

@Component({
  selector: 'bill',
  templateUrl: './bill.component.html',
  styles:[`
    .header{
      left: 0;
      right: 0;
      top: 0;
      height: 130px;
      position: fixed;
      z-index: 2;
    }
    .footer{
      position: fixed;
      bottom: 0;
      left: 0;
      right: 0;
    }
    .sidebar{
      position: fixed;
      top: 130px;
      left: 0;
      width: 300px;
    }
    main{
      position: fixed;
      left: 300px;
      top: 130px;
      height: 490px;
      right: 0;
      overflow-y:scroll;
    }
    table.dataTable thead th, table.dataTable thead td {
        padding: 10px 18px;
        border-bottom: 1px solid #ddd !important;
    }
    .sv_tab table thead th {
        background: #ddd !important;
    }

  `]
})
export class BillComponent {
  constructor(private router:Router){
    this.router.navigateByUrl('bill/(inner:list)');
    // this.router.navigate([ 'bill', {outlets:{inner:['list']}}]);
  }
   
}
