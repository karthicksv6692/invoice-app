import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'customer-add',
  templateUrl: `./customer_add.component.html`,
})
export class CustomerAddComponent {
    customerName = new FormControl();
}
