import { Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

@Component({
  selector: 'dashboard',
  templateUrl: './dashboard.component.html',
  styles:[`section.foot_panels {
    position: fixed;
    bottom: 0;
    width: 100%;
}`]
})
export class DashboardComponent {
}
