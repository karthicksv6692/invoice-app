import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule }  from '@angular/router';

import { HeaderOneComponent } from '../common/header1.component';
import { HeaderTwoComponent } from '../common/header2.component';
import { HeaderThreeComponent } from '../common/header3.component';
import { TopMenuComponent } from '../common/top-menu.component';
import { SideBarComponent } from '../common/sidebar.component';
import { FooterComponent } from '../common/footer.component';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { BillComponent } from '../dashboard/bill.component';
import { BillAddComponent } from '../dashboard/bill-add.component';
import { BillProductComponent } from '../dashboard/bill-product.component';
import { BillSingleComponent } from '../dashboard/bill_single.component';
import { CustomerAddComponent } from './customer/customer_add.component';
import {SelectModule} from 'ng2-select';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import { BillService } from './shared/bill.services';
import { BillListComponent } from './bill-list/bill-list.component';

import { Ng2TableModule } from 'ng2-table/ng2-table';
import { NgTableExampleComponent } from './ng-table-example/ng-table-example.component';
import { PaginationModule } from "ng2-bootstrap/pagination";
//import { NgTableComponent, NgTableFilteringDirective, NgTablePagingDirective, NgTableSortingDirective } from 'ng2-table/ng2-table';

@NgModule({
    declarations: [
      HeaderOneComponent,HeaderTwoComponent,TopMenuComponent,FooterComponent,DashboardComponent,SideBarComponent,BillComponent,BillAddComponent,HeaderThreeComponent,BillProductComponent,BillSingleComponent,CustomerAddComponent, BillListComponent,NgTableExampleComponent
    ],
    imports: [
      BrowserModule,RouterModule,SelectModule,FormsModule,ReactiveFormsModule,Ng2TableModule,PaginationModule.forRoot()
    ],
    providers: [BillService],
    entryComponents: [
      BillProductComponent
    ]
    
  })
  export class DashboardModule { }