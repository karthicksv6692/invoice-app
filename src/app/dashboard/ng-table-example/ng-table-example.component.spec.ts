import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgTableExampleComponent } from './ng-table-example.component';

describe('NgTableExampleComponent', () => {
  let component: NgTableExampleComponent;
  let fixture: ComponentFixture<NgTableExampleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NgTableExampleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NgTableExampleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
