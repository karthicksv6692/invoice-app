import { Injectable } from '@angular/core';
import { ISubscription } from 'rxjs/Subscription';
import { Subject } from 'rxjs/Subject';

@Injectable()

export class BillService{

    setLatestData:ISubscription=new Subject<any>();

    getCustomer(){
        var fruits = [];
        customer.forEach(function(event) {
            fruits.push(event.name);
        });
        return fruits;
    }
    getEmployee(){
        return employee;
    }
    getPaymentType(){
        return payment_type;
    }
    getInvoiceNo(){
        if(!localStorage.table_data){
            return `RE18-${invoice_number+1}`;
        }
        
        var ar = JSON.parse(localStorage.table_data);
        
        if(ar){
            invoice_number = ar.length
        }else{
            invoice_number = 0;
        }
        return `RE18-${invoice_number+1}`;
    }
    getProjectNo(){
        return project_number;
    }
    getSingleCustomer(id){
        var i=0;
        for (i = 0; customer.length > i; i++) {
            if (customer[i].name == id) {
                return customer[i]['customer-number'];
            }
        }
        return 0;
    }
    getSingleBill(id){
        var i=0;
        var ar = JSON.parse(localStorage.getItem('table_data'));
        for (i = 0; ar.length > i; i++) {
            if (ar[i].invoice_number == id) {
                return ar[i];
            }
        }
        return ar[id];
    }
    getSingleProductDetails(id){
        var i=0;
        for (i = 0; products.length > i; i++) {
            if (products[i].id == id) {
                return products[i];
            }
        }
        return 0;
    }
    getTodayDate(){
        let date = new Date();
        let day = date.getDate();
        let month = date.getMonth() + 1;
        let year = date.getFullYear();
        if (month < 10) month = 0 + month;
        if (day < 10) day = 0 + day;
        return  month + "-" + day + "-" + year;
    }
    getProducts(){
        return products;
    }
    getProductNames(){
        var fruits = [];
        products.forEach(function(event) {
            fruits.push({id:event.id,text:event.name});
        });
        return fruits;
    }
    
}

const customer = [
    { id:1, name: 'Customer-1','customer-number':'SINB101' },
    { id:2, name: 'Customer-2','customer-number':'SINB102' },
    { id:3, name: 'Customer-3','customer-number':'SINB103' },
    { id:4, name: 'Customer-4','customer-number':'SINB104' },
];

const products = [
    { id:1, name: 'Mobile','price':100,'desc':'MobileMobileMobile' },
    { id:2, name: 'LED Tv','price':200,'desc':'LED TvLED TvLED TvLED TvLED Tv' },
    { id:3, name: 'Router','price':300,'desc':'RouterRouterRouterRouter' },
    { id:4, name: 'Watch','price':400,'desc':'WatchWatchWatch' },
];

const employee = [
    { id:1, name: 'Muthu' },
    { id:2, name: 'Karthick' },
    { id:3, name: 'Siva' },
    { id:4, name: 'Dev' },
];

const payment_type = [
    { id:1, type: 'NEFT' },
    { id:2, type: 'Paypal' },
    { id:3, type: 'Manual' },
    { id:4, type: 'Other' },
];

let invoice_number = 0;
const project_number = 'PRO18-0001';